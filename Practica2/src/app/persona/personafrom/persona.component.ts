import { Component, OnInit } from '@angular/core';
import { Usuario } from '../persona.module';

@Component({
  selector: 'yoshi-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})
export class PersonaComponent implements OnInit {

  public usuarioRegistro: Usuario = new Usuario(null, null);
  public listaUsuarios: Array<Usuario> = [];

  constructor() { 
    console.log("constructor ejecutado");
  }

  ngOnInit() {
    console.log("componente ejecutado");
  }

  registroUsuario(usuario:Usuario) {
    this.listaUsuarios.push(usuario);
  }

}

