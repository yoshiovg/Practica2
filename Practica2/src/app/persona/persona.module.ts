import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PersonaComponent } from './personafrom/persona.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule
    ],
    declarations: [PersonaComponent],
    exports: [PersonaComponent]
  })
export class PersonaModule { }
  

export class Usuario {
  nombre: string;
  apellido: string;

  constructor(nombre:string, apellido:string){
      this.nombre = nombre;
      this.apellido = apellido;
  }
}